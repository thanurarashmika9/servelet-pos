package lk.ijse.absd.servlets.repository.spec;

import lk.ijse.absd.servlets.entity.Item;
import lk.ijse.absd.servlets.repository.other.SuperRepo;

import java.sql.SQLException;

public interface ItemRepo extends SuperRepo<Item,Integer> {

    /*int getCount() throws SQLException;*/
}
